# Social Network App Project Overview

## User Manual

### Introduction
Social Network App is an App designed both for the patients with liver diseases and their carepartners to manage their social networks. 
<br> In this App, users can add/delete contacts to their accounts. Patient users has a graph produced based on their contact frequencies and closeness to their caregivers and the graph will be displayed on the main page. Patient users can also send an alert to a carepartner to inform they need assistance. In addition, all users that have account should be able to view their PDF report which contains their basic information, social relationships, and social network graphs.
<br> More detailed descriptions and instructions are listed below.

### Register and Login
When open this App, users should see the register/login page. Users can input their username and password to login, or they can click on 鈥淣EW USER? REGISTER HERE.鈥?button to register a new account to login.

<img src="figures/login.png" width="45%" height="40%">
<img src="figures/register.png" width="40%" height="40%">

After login, the user will see the main menu on the screen. 
<br> <img src="figures/main_menu.png" width="40%" height="40%">
<br>
<br>
### View Graph / Generate Report
<br> After successfully login, patient users should see a page which displays her/his social network graph which represents the contact frequencies and closeness to her/his contacts. A red circle button on the left top of the screen can be clicked to generate a PDF report. After clicking this button, the screen will be freezed about 10 sec and then a PDF file will be displayed. 
<br> During this freezing period, the App will take a screenshot on the graph. Thus, it鈥檚 recommended to drag the graph on the center of the page before clicking the PDF button in order to having the graph being displayed in a perfect position on the PDF page.
<br> There will be two pages in the PDF file. The first page contains the patient user鈥檚 basic information and the personal information of her/his closest carepartners, and the second page contains a big picture of the patient鈥檚 social network graph, which was captured in the graph page.

<img src="figures/graph.png" width="45%" height="40%">
<img src="figures/generate_pdf.png" width="50%" height="40%">

### Add Node in Graph
<br> This function can add third-party stakeholders that neither belong to patients nor carpartner to the patient users鈥?social network graph.
<br> Once clicking 鈥淎dd Node鈥?button, a new page will be popped-up which will show the current contact list in the user's phone. The user may choose one from the list as his/her new node in the graph to add. If the want-to-add user does not exist in his/her current contact, the user may click the "add new contact" button to add a new node.
After clicking 鈥淥K鈥? this page will be destroyed with a new node created and then the users are expected to view a updated graph on the graph page.

<img src="figures/add_node.png" width="40%" height="40%">

### Edit Node
<br>Once clicking 鈥淓dit Node鈥?button, a new page with the information of that existing contact will be popped-up for the users to edit. After clicking 鈥淥K鈥? this page will be destroyed and the information of that contact will be updated.

### Delete Node
<br> Once clicking 鈥淒elete Node鈥?button, a new page with the names of all existing contacts will be popped-up for the users to select. After clicking 鈥淥K鈥? this page will be destroyed and all selected contacts will be destroyed on the graph. The users are expected to view a updated graph on the graph page.

### Adding A Care-Partner to Your Account
<br> A care-partner may be added by clicking the 鈥淎dd Care-Partner鈥?button under the graph. This will allow you to enter the name and email id of the care-partner. After clicking the 鈥淥K鈥?button, an email will be sent to the provided address asking your care-partner to register using the provided mail id using the link provided.
<br> The carepartner can follow the instructions on the email to login/register her/his account to complete the confirmation process. Once completed, a new node with the carepartner鈥檚 information will be added to the graph and the users can view the graph after refreshing the graph page on the App.

### CarePartner Registration/Login
When an email id is added as a Care-Partner in a user account, this person receives an email to their account with a link that allows them to create an account. Care-partners must use the link provided in the email for registration to be able to access the patient account. After registration is successful, please login to the android application using the same credentials (email id and password) used to create the care-partner account following these credentials.

### Send Email
This function is only useful for partner users. Once clicking 鈥淪end Alert鈥?button, a new page with the names of all carpartner contacts will be popped-up for the users to select. After clicking 鈥淥K鈥? the page will be destroyed and an email will be sent to the selected carepaterner(s) to inform that this patient is asking for support.

<img src="figures/send_alert.png" width="35%" height="40%">
<img src="figures/receive_email.png" width="80%" height="40%">

<br>
<br>
<br>

## Support Manual

### Introduction
This part is the support maunal for the developer of the Social Network App. Social Network App is designed for those who are suffering from the end-stage liver diseases and want to have some connections with their social networks, for connections are an indispensable part during the whole life. 


### Front-end (Android App)

#### Installaion and Configuration
1. Unzip social_network_final.zip and open the project with Android Studio. The project setup will sync automatically.
2. All the java sources codes are mainly located in the folder ./app/src/main/java, and it contains two subfolders - chixiang.pdf_module and kaiwang.social_network. It contains many .java submodules to realize different functions which will be introduced more specifically in later sections.
3. As for the front-end user interface layout, each activity has it own corresponding layout in this scenario. All the layout files are located to ./app/src/main/res/layout.

#### Code Structure
The front-end android app is made of several basic activities and the corresponding activity_layout. The project of social_network android app is like this:
<img src="figures/front_end_structure.png" width="80%" height="40%">

#### APIs
In the development of front-end android app, we make use of several APIs.
*ContactsContract*: ContactsContract defines an extensible database of contact-related information. Contact information is stored in a three-tier data model:

1. A row in the **Data** table can store any kind of personal data, such as a phone number or email addresses. The set of data kinds that can be stored in this table is open-ended. There is a predefined set of common kinds, but any application can add its own data kinds.
2. A row in the **RawContacts** table represents a set of data describing a person and associated with a single account (for example, one of the user's Gmail accounts).
3. A row in the **Contacts** table represents an aggregate of one or more RawContacts presumably describing the same person. When data in or associated with the RawContacts table is changed, the affected aggregate contacts are updated as necessary.

*WebView*: WebView objects allow you to display web content as part of your activity layout, but lack some of the features of fully-developed browsers. A WebView is useful when you need increased control over the UI and advanced configuration options that will allow you to embed web pages in a specially-designed environment for your app.

Other APIs: <br>
*AndroidPdfViewer* (https://github.com/barteksc/AndroidPdfViewer) <br>
*PdfBox-Android* (https://github.com/TomRoush/PdfBox-Android) <br>
*WebChromeClient* (https://developer.android.com/reference/android/webkit/WebChromeClient) <br>
*WebView* (https://developer.android.com/reference/android/webkit/WebView) <br>
*WebViewClient* (https://developer.android.com/reference/android/webkit/WebViewClient) <br>



### Back-End

The back-end code for our project is split across two servers: one to perform user authentication and another to provide access to the graph store. Both these parts communicate with each other and the android client using HTTP calls (with JSON data as needed).

The authentication server provides basic account creation, login / authentication, and account deletion. This data is managed using a MySQL database, and access is served by a Tomcat 9 application. The server-side code is Java, making use of the standard mysql JDBC driver. 

The graph server allows for data storage: the patient's information and those of the people they added in the application. The graph data is held in a Neo4J database, which is better at handling link and node storage, unlike MySQL. The server-side code is also Java, but it makes use of a Cypher (Neo4J's query language) driver instead.  Access is also served by a Tomcat 9 server.

#### Graph Data Structure

As stored in Neo4j, a graph, representing a social network, includes both nodes and relationships.

Nodes represent the individuals, and store different information depending on the role that person plays. The following table details these differences.

|  |  Patient  |  Care Partner | Other Helpers  |
|------------------------------|:-----------------------------------------------------------:|:-----------------------------------------------------------------------:|:-------------------------------------------------------------:|
| Fields    | First Name Last Name E-Mail Phone Number Thumbnail UserName | First Name Last Name E-Mail Phone Number Thumbnail UserName HelperIds* | First Name Last Name E-Mail Phone Number Thumbnail HelperId |
| Has Account with Edit Access |   Yes   | Yes | No   |


Relationships represent the way that individuals know each other. There are two types of relationships, as detailed in the following table:

|      | Care Relationship                  | Other Relationship           |
|------|------------------------------------|------------------------------|
| From | Patients                           | Care Partners, Other Helpers |
| To   | Care Partners, Other Helpers       | Care Partners, Other Helpers |
| Data | Survey Data Nature of Relatinoship | Nature of Relationship       |

Survey data includes information that is queried from patients, such as how often they speak with the patient and how much the patient feels comfortable discussing health issues with that individual.

There are a small number of additional rules in our graph:

1) Patients are identified by their username. Non-patients are identified by a helperId that relates them to a given patient
2) A care-partner can help out multiple patients (and as such have multiple helperIds)
3) Patients can have multiple care-partners
4) Every non-patient node is connected to a patient
5) Non-patient nodes may or may not have any connections to other non-patient nodes
6) Other helper nodes can be promoted to a care-partner only by patients
7) Care partners can be demoted back to other helper nodes, again only by patients

An example of how this data is represented in the server appears below (note that this is different from the visualization that appears in the application itself, as this contains data on ALL the individuals in the database, not just the ones relevant for a given patient).

<img src="figures/Neo4j_Example.png" width="60%" height="50%">

#### Authentication Data Structure

The authentication data is more similar to what is typically stored in most applications.  There are three relevant tables involved here:

|  | AccountInfo | Roles | Sessions |
|---------------|-------------------------------------------------------|--------------------------------|----------------------------------------------------------|
| Role | Stores information about accounts | Look up table for the accounts | Tracks current android sessions to limit database access |
| Fields | UserName, Hashed Password, Password Salt, Email, Name | UserName, PatientName, Email | SessionId, UserName |
| Rows per user | One | At least one | One |

Note that roles can have multiple rows per user, as care partners can have multiple rows if they are connected to multiple patient. Also, all of this data is only relevant to individuals with accounts (Patients and Care Partners). Other helpers only exist in the graph data. The sessions table allows handling authentication by each call. It also ensures that individuals' sessions in the app are not indefinite.

#### Code Structure

This section details the organization and functionality of the server-side code. Both were built using Maven, under the simple webapp archetype. The POM in each of these projects specifies output to a WAR file. 

##### Graph Server

The project structure for the graph server is as follows:

<img src="figures/graph_server.png" width="55%" height="40%">

The packages involved are as follows:

* The Data package contains the representation of all of the information that appears in the graph. These include Nodes (patient, care-partner, and other helper) and relationships (care and other). Graph is an object that contains all the relevant nodes and relationship for a given patient, for convenience of reading and writing by the android application. Each of these object types can be written to or built from a JSON object that includes all the necessary fields, a feature that is heavily used by the web service packages. It also includes exceptions for the failure to parse these representations properly. Finally, there is a Thumbnail object class, which is used to handle reading and writing of images through conversation to a base 64 string. 

* The N4j package contains wrappers for the Neo4j Cypher driver. The main class here is CypherEngine, which includes functions to create, remove, edit, and query nodes and relationships  FullEngine and AnalysisEngine each make use of CypherEngine. The former allows queries of the full graph, into either the standard JSON representation (as defined in the Data package), or as a D3 JSON (D3 is a javascript package for making plots, which requires a specific format). The latter includes a number of calls for analyzing the data in the graph, such as ranking the closest helpers or counting the number of relationships in a patient's network. 

* The WebService contains definitions of the various REST calls that can be made. Each of the functions in these classes builds an engine from the previous package, and then calls the required functions to achieve the necessary goal. For a full list of the capabilities available through these REST calls, see the APIs section below.

Additional files include.

* The test section includes classes for testing each of the various objects and calls to the server. The data classes test JSON reading and writing. The N4J tests handle the various calls to the engine classes to test node creation, editing and querying 

* The json and img folders contain data files used for the tests.

* WEB-INF contains files that are used by Tomcat to set-up the the various API paths properly.

* UML contains class diagrams demostrating how the various classes used fit togehter 

##### Authentication Server

The structure of the authentication server project is as follows. It follows a similar general plan as the graph server, albeit somewhat simpler as more of this functionality is standard across applications.

<img src="figures/auth_server.png" width="55%" height="40%">

* The Data package holds representation of the account info and registration information that will pass from the android application to the server. All the data objects read and write data to JSON, and are used by the other packages to access the server. Note that in line with security best practices, none of the account data is stored in the server state.

* CreateTables_Authentications.sql is the set of SQL queries to establish each of the needed tables in MySQL.

* MySQLConnection is a wrapper for the MySQL driver, containing the specifics to login to and access the tables created in the above script.

* Register contains the functionality to create a user. It accepts the relevant data for the account info and roles tables. The password is accepted pre-hashed, is salted and hashed, and stored in the account info table (this functionality is borrowed from the UserAuth package). This code also includes the functionality to receive a newly upgraded care partners information and set them up. This includes sending an e-mail and HTML form to the new user to set them up with the appropriate access. 

* UserAuth contains functions to check if a given set of credentials matches an existing user in the table, allowing for login in users, as well as for seeing if a to-be-added user is already in the table. 

* Sessions contains functionality to establish a "session" on the server. To do so, it returns a cookie to the client, and creates an entry in the sessions table. Both these are ephemeral and will be deleted after a specified number of time. This is the primary source of interaction between the authentication and graph servers. Every call to the graph server involves a call to the authentication server via the sessions to confirm that there is still an active session owned by the android client.

* As above, the WebService contains definitions of the various REST calls that can be made. For a full list of the capabilities available through these REST calls, see the APIs section below.

#### Code Location

Our code is currently stored in gitlab at gitlab.oit.duke.edu/SocialNetworks. Currently, the branches reflect our state of collaborative development across a team of 8 coders. It is also somewhat disorganized (there are still some projects that contain our experimentation with various packages and architectures) This will be cleaned prior to delivery, and will be consolidated into the three projects described here (AndroidClient, GraphServer, and AuthenticationServer). Furthermore, all of the git trees will be defined into a main, development, and deployed branches. 

If requested, we can transfer our projects to a github specified by the hospital. However, we believe this location is already accessible by the School of Nursing and is likely the easier place to leave the code. If so, we will invite the relevant members of the Nursing IT team as project owners.

#### AWS Server

Our server code currently runs on a pair of AWS EC2 machines (one for each server-component). Each is an Ubuntu 18.04 image, SDD Volume with 64-bit architecture. Each is sized at Amazon's M4.large (8 GB of memory).  The authentication server runs at address ec2-18-224-67-74, while the graph server runs from ec2-18-188-17-13. The 8080 ports on these are currently open to any IP for testing; obviously this is not the best security practice, but all of our data is only test data at this point. Part of establishing containers (see the next section) for this code will be determining a more secure way of hosting.

The packages / software that will need to be containerized are...

1) Tomcat Server 9
2) Java 8 JRE
3) MySQL
4) Neo4J Community Edition
5) Authentication WAR (packaged with all dependencies)
6) Graph Server WAR (packaged with all dependencies)

There were additional software downloaded to these machines for development purposes. To allow our team to code with an IDE, each was set up with Eclipse proton, as well as a VNCserver and XVNC desktop. 

We will be maintaining these machines through the end of June in case any immediate changes need to be made to our delivered code. After this point, they will be retired and deleted. 

#### Docker
*Note: We are still working on setting up docker with the Duke School of Nursing; we want to be sure everything works on their system and will be meeting with them this week to determine our best courses of action. This document will be updated accordingly*
There will be two Docker container stacks for the server components. One for the authentication server and one for the Graph Server. Each container contains its own database: MySQL for Authentication and Neo4J for the Graph. Each should be able to be run using docker-compose up. Both need to be running for the application to work. The docker configuration must be modified to reflect the server each is running on. The auth server must respond to HTTP requests from the android application and the graph server. Similarly the graph server must respond to HTTP requests from the android application. The docker compose files for each must also import and setup images for a Tomcat Server to enable this functionality.

#### APIs
We have many APIs to connect to neo4j and edit the graph in neo4j database. I will list all of them below.
<br>*CarePartnerWS*: You can use this API to add a carepartner to the neo4j database, judge if a carepartner is connected to some patients, get the carepartner node by its id, delete a carepartner by its id and update a helper to be a carepartner.
<br>*Closest3WS*: You can use this API to get the closest 3 helper of a patient. We can use this result for future matching.
<br>*HelperWS*: You can use this API to judge if a patient is connected to some helpers, get a helper node by its helper ID, add a new helper to the graph, delete a helper by its helper ID and update a helper to be a carepartner.
<br>*PatientWS*: You can use this API to add a patient to the graph, judge if a patient of specific ID exist, get a patient node by its ID, delete a patient node by its patient ID.
<br>*RelationshipWS*: You can use this API to add a relationship, delete a relationship and match a specific relationship.

#### Tests
We use some test cases to test the availability of the connection to the neo4j database. Each API has an unique url and we use this url to test our APIs. 

### Additional Files
#### Social network app requirement 
The document contains the detailed functional and non-functional requirements of the Mobile app.
#### Architecture document 
The document contains the system architecture diagram from C4 and the use case diagram introducing the functions in the app.
#### Test plan
The document contains detailed information of how the app will be tested from methodology, personnel and timing plan. The methodology introduced three kinds of testing: unit testing, component testing and system testing. Tasks are divided in the following part. Timing plans for sprint 1 and sprint 2 are in the last part.

